#!/usr/bin/env python3

from pytm.pytm import TM, Server, Datastore, Dataflow, Boundary, Actor, Lambda, Data, Classification

tm = TM("Context Diagram")
tm.description = "Context Diagram of Skysales store"


# External Entities
user = Actor("User")
admin = ...

# Processes
store = Server("Skysales ticket store")

# Data Flows
user_to_store = Dataflow(user, store, "Get tickets")

admin_to_store = ...

tm.process()

