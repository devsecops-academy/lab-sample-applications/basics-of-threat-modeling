#!/usr/bin/env python3

from pytm.pytm import TM, Server, Datastore, Dataflow, Boundary, Actor

tm = TM("Data Flow Diagram")
tm.description = "Data Flow Diagram with trust boundaries"

# Trust boundaries
Users_Store = Boundary("Users/Store")
Payment_Gateway_DB = ...
Store_Payment_Gateway = ...


# External Entities
user = ...
admin = Actor("Admin")

# Assign trust boundaries
user.inBoundary = Users_Store
admin.inBoundary = ...

# Processes
store = ...
payment_gateway = Server("Payment Gateway")

# Assign trust boundaries
store.inBoundary = Store_Payment_Gateway
payment_gateway.inBoundary = ...

#Data store
db = ...
db.inBoundary = Payment_Gateway_DB

#Data flow
user_to_store = ...
admin_to_store = Dataflow(admin, store, "Get customer data")
store_to_payment_gateway = ...
payment_gateway_to_db = Dataflow(payment_gateway, db, "Save Data")


tm.process()

